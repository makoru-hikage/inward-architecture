module.exports = function AjvErrorMessageMaker(validation) {
    // Extract error data from ajv to
    // determine which of the attributes
    // have caused HTTP 400
    let errorDetails = {}

    validation
    .map(v => {
        let fieldName = v.dataPath.slice(1)
        let message = v.message
        let keyword = v.keyword

        if (keyword === 'required'){
        fieldName = v.params.missingProperty
        message = fieldName + " is required."
        }

        if (keyword === 'additionalProperties'){
        let additionalProperty = v.params.additionalProperty
        fieldName = 'additionalProperties'

        if (errorDetails.hasOwnProperty(fieldName)){

            errorMessagesString = errorDetails['additionalProperties'] + ', '+ additionalProperty
            message = "Please remove the following: " + errorMessagesString
        } else {
            message = "Please remove the following: " + additionalProperty
        }

        }
        errorDetails[fieldName] = message

    })

    return errorDetails
}