const Ajv = require ('ajv')
const AjvErrors = require ('ajv-errors')

module.exports = function AjvSchemaCompiler () {
  // Create a custom ajv instance to use the ajv-errors lib
  const ajv = Ajv({
    allErrors: true, 
    jsonPointers: true,
    $data: true
  })

  AjvErrors(ajv)

  return (schema) => {
    return ajv.compile(schema)
  }
}