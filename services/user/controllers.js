const Sequelize = require ('./adapters/Sequelize')

const RegisterHandler = require ('./application/RegisterHandler')

module.exports = {
  RegisterController: async function (request, reply) {
    await RegisterHandler(Sequelize)
      .run(request.body)
      .then(data => reply.send(data))

  }
}
