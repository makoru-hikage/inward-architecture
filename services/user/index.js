const AjvSchemaCompiler = require ('./adapters/AjvSchemaCompiler')
const AjvErrorMessageMaker = require ('./adapters/AjvErrorMessageMaker')

const routes = require('./routes')

const SERVICE_URL_PREFIX = '/users'

// Used for handling 4xx errors
const errorHandler = function (error, request, reply){
  console.log(error)
  let errorBody = { 
    error: error.error,
    message: error.message,
    details: error.details
  }

  if (error.validation) {
    let validation = error.validation
    errorBody.error = "Bad Request"
    errorBody.message = "Invalid or incomplete input"
    errorBody.details = AjvErrorMessageMaker(validation)
  }

  reply.send(errorBody)

}

// Initiate the routes and use ajv and ajv-handler
module.exports = function (fastify, opts, next) {

  fastify.setErrorHandler(errorHandler)

  for (const route of routes) {
    route.schemaCompiler = AjvSchemaCompiler()
    route.url = SERVICE_URL_PREFIX + route.url
    fastify.route(route)
  }
  next()
}