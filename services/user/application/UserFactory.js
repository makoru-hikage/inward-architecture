/**
 *
 * @module module:application/UserFactory
 * @typedef {Object} UserFactory
 * @typedef {Object} User
 */

const User = require('./domain/User')

/**
 *
 * @constructs UserFactory
 * @return {UserFactory}
 */
module.exports = function UserFactory () {
  let factory = {}

  return Object.assign(
    factory,
    creation(factory)
  )
}

/**
 * A Factory is meant to create an Object
 *
 * @param {UserFactory} factory 
 */
const creation = (factory) => ({
  /**
   * 
   * @param {Object} params The data body to supply the values of the User's attributes
   * @return {User}
   */
  create: function (params = {}) {

    // Only certain attributes will be persisted to the database
    let attributes = {
      phone_no: params.phone_no,
      username: params.username,
      email: params.email,
      password: params.password,
      salt: params.salt,
      first_name: params.first_name,
      last_name: params.last_name,
      date_of_birth: params.date_of_birth,
      city: params.city
    }

    let user = User (attributes)

    // Return the data to be persisted in a DB
    return user
  }
})
