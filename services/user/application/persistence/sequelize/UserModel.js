const Sequelize = require ('sequelize')

module.exports = function User (sequelize) {
  return sequelize.define('users', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    phone_no: Sequelize.STRING,
    username: Sequelize.STRING,
    email: Sequelize.STRING,
    password: Sequelize.STRING,
    salt: Sequelize.STRING,
    first_name: Sequelize.STRING,
    last_name: Sequelize.STRING,
    date_of_birth: Sequelize.DATEONLY,
    gender: Sequelize.STRING,
    city: Sequelize.STRING
  }, {
    schema: 'app',
    underscored: true
  })
}