const User = require('./UserModel')

module.exports = function UserPersistenceService (persistenceAdapter) {
  let service = {
    persistenceAdapter
  }

  return Object.assign(
    service,
    persistence(service)
  )
}

const persistence = (service) => ({

  insert: async function (input) {
    let persistenceAdapter = service.persistenceAdapter

    return User(persistenceAdapter).create(input)
  },

  findOne: async function (uniqueColumn, value) {
    let persistenceAdapter = service.persistenceAdapter
    let where = {}
    where[uniqueColumn] = value

    return User(persistenceAdapter).findOne({
      where: where
    })
  }
})
