/**
 * Receives and processes data from a Controller:
 * A handler for user registration
 *
 * @module module:application/RegisterHandler
 * @typedef {Object} RegisterHandler
 * @typedef {Object} PersistenceAdapter
 */

const bcrypt = require ('bcrypt')

const UserFactory = require ('./UserFactory')
const UserRepository = require ('./UserRepository')
const UserValidation = require ('./UserValidation')

/**
 *
 * @constructs RegisterHandler
 * @param {PersistenceAdapter} persistenceAdapter
 * @return {RegisterHandler}
 */
module.exports = function RegisterHandler (persistenceAdapter) {
  let handler = {
    factory: UserFactory(),
    repository: UserRepository(persistenceAdapter)
  }

  return Object.assign(
    handler,
    runnable(handler)
  )
}

/**
 * A Handler must run
 *
 * @param {RegisterHandler} handler 
 */
const runnable = (handler) => ({
  /**
   * @async
   * @param {Object} input Preferably the Body of a POST Request
   * @return {Object} It can be used to supply a Response Body
   */
  run: async function (input) {
    let validatedInput = await UserValidation(input).validate()

    // Save the password in a hashed form
    await bcrypt.hash(validatedInput.password, 10)
      .then((result) => validatedInput.password = result)

    let user = handler.factory.create(validatedInput)
    let result = handler.repository.insert(user)

    return result
  }
})

const userExistenceCheck = (handler) => ({
  checkByUsername: async function (value) {
    return handler.repository.findOne("username", value)
  },
  checkByEmail: async function (value) {
    return handler.repository.findOne("email", value)
  },
  checkByPhoneNumber: async function (value) {
    return handler.repository.findOne("phone_no", value)
  }
})
