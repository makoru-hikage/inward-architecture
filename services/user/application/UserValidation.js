/**
 * An Object to validate parameters for creating a User
 *
 * @module module:application/UserValidation
 * @typedef {Object} UserValidation
 * @typedef {Object} PasswordObject
 */

const PasswordObject = require ('./domain/value_objects/Password')

/**
 *
 * @constructs UserValidation
 * @param {Object} params
 * @returns {UserValidation}
 */
module.exports = function UserValidation (params) {
  let validation = {
    params
  }

  return Object.assign(
    validation,
    process(validation)
  )
}

/**
 * 
 * @param {UserValidation} validation The process of validation
 */
const process = (validation) => ({
  /**
   * @async
   * @return {Objects} returns the params when it is valid
   */
  validate: async function () {
    let input = validation.params
    let errorMessages = {}

    // Check if the Phone Number is a Philippine number
    let thePhoneNumberIsNotValid = ! input.phone_no.startsWith('+63')
    
    if (thePhoneNumberIsNotValid){
      errorMessages['phone_no'] = 'The phone number is not valid'
    }

    // Server-side Password Confirmation
    let passwordObject = PasswordObject(input.password)
    let repeatedPassword = input.confirmPassword
    let passwordsDoNotMatch = ! passwordObject.confirmPassword(repeatedPassword)

    if (passwordsDoNotMatch){
      errorMessages['confirmPassword'] = 'The passwords don\'t match'
    }

    if (Object.entries(errorMessages).length > 0) {
      return Promise.reject({
        error: "Unprocessable Entity",
        message: "Invalid input",
        statusCode: 422,
        details: Object.assign(errorMessages)
      })
    }

    return validation.params
  }
})