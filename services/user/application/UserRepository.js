/**
 * Responsible for data persistence and querying:
 * A repository object for user database
 *
 * @module module:application/UserRepository
 * @typedef {Object} UserRepository
 * @typedef {Object} User
 */

const UserPersistenceService = require ('./persistence/sequelize/UserPersistenceService')

/**
 *
 * @constructs UserRepository
 * @param {PersistenceAdapter} persistenceAdapter
 * @return {UserRepository}
 */
module.exports = function UserRepository (persistenceAdapter) {
  let repository = {
    persistenceService: UserPersistenceService(persistenceAdapter)
  }

  return Object.assign(
    repository,
    persistence(repository)
  )
}

/**
 * Allows a repository to do a DB write, update, and delete
 *
 * @param {UserRepository} repository 
 */
const persistence = (repository) => ({
  /**
   * Records a new User into the DB
   *
   * @param {User} user
   * @return {Object} The user's data
   */
  insert: async function (user) {

    let persistenceService = repository.persistenceService
    console.log(persistenceService)

    persistenceService.insert(user)

    // Return the data to be persisted in a DB
    return user
  }
})
