/**
 * A Domain Model for password-related operations
 * 
 * @module module:application/domain/value_objects/Password
 * @typedef {Object} Password
 */

/**
 * Generally a function to check if a particular record exists in a DB
 * 
 * @callback hashingFunction
 * @param {string} password a plaintext password to be hashed
 * @return {string} the password hash
 */

/**
 * @constructs Password
 * @param {string} password A plaintext password
 * @return {Password}
 * 
 */
module.exports = function Password (password = '') {
  let service = {
    password
  }

  return Object.assign(
    service,
    canConfirmPassword(service),
    passwordChecking(service)
  )
}

/**
 * Allows the comparison of plaintext passwords
 *
 * @param {Password} self 
 */
const canConfirmPassword = (self) => ({
  /**
   * Check if two passwords match
   *
   * @param {string} repeatedPassword
   * @return {boolean}
   */
  confirmPassword (repeatedPassword = '') {
    return self.password === repeatedPassword
  }
})

/**
 * Allows to hash a plaintext password and compare it to a stored hashed password
 *
 * @param {Password} self 
 */
const passwordChecking = (self) => ({
  /**
   * Hash the Password's value to check against an existing User's hashed password
   *
   * @param {Object} user The user who has the password hash
   * @param {Function} hashingFunction The function used to hash this object's value
   * @return {boolean} Does it match?
   */
  checkPassword (user, hashingFunction) {
    return hashingFunction(self.password) === user.password
  }
})