/**
 * Represents a user of this app
 *
 * @module application/domain/User
 * @typedef {Object} User
 */

/**
 * Generally a function to check if a particular record exists in a DB
 * 
 * @callback uniquenessCheckFunction
 * @param {string} uniqueId a single-field unique identifier
 * @return {boolean} true if the User exists
 */

/**
 *
 * @constructs User
 * @param {Object} attributes
 * @return {User} 
 */
module.exports = function User (attributes) {
  let user = {
    phone_no: attributes.phone_no,
    username: attributes.username,
    email: attributes.email,
    password: attributes.password,
    salt: attributes.salt,
    first_name: attributes.first_name,
    last_name: attributes.last_name,
    date_of_birth: attributes.date_of_birth,
    gender: attributes.gender,
    city: attributes.city
  }

  return Object.assign(
    user,
    fullyNamed(user),
    uniqueness(user),
    hasBirthday(user)
  )
}

/**
 * The User must have a first and last name
 * 
 * @param {User} user 
 */
const fullyNamed = (user) => ({
  /**
   * Get the full name of the user
   *
   * @return {string} - the full name of the user
   */
  getFullName () {
    let fn = user.first_name
    let ln = user.last_name
    return fn + ' ' + ln
  }
})

/**
 * A User has an age
 *
 * @param {User} user 
 */
const hasBirthday = (user) => ({
  /**
   * Get the age of the user
   * 
   * @return {string} - the age of the user in years
   */
  getAge (ageCalculator) {
    let birthday = user.date_of_birth
    return ageCalculator(birthday)
  }
})

/**
 * Used to uniquely identify a User instance
 *
 * @param {User} user 
 */
const uniqueness = (user) => ({
  /**
   * Check if user exists using their email
   *
   * @param {Function} uniquenessCheckFunction
   * @return {boolean} Does the user exist?
   */
  checkIfEmailExists (uniquenessCheckFunction) {
    return uniquenessCheckFunction(user.email)
  },

  /**
   * Check if user exists using their username
   *
   * @param {Function} uniquenessCheckFunction
   * @return {boolean} Does the user exist?
   */
  checkIfUsernameExists (uniquenessCheckFunction) {
    return uniquenessCheckFunction(user.username)
  },

  /**
   * Check if user exists using their phone number
   *
   * @param {Function} uniquenessCheckFunction
   * @return {boolean} Does the user exist?
   */
  checkIfPhoneNumberExists (uniquenessCheckFunction) {
    return uniquenessCheckFunction(user.phone_no)
  }
})
