const { RegisterController } = require('./controllers')
const UserRegisterSchema = require('./schemata/user_register.json')

module.exports = [
    {
      method: 'POST',
      url: '/',
      handler: RegisterController,
      schema: UserRegisterSchema
    }
  ]