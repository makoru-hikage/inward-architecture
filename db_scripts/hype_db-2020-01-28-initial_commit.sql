-- Database generated with pgModeler (PostgreSQL Database Modeler).
-- pgModeler  version: 0.9.2
-- PostgreSQL version: 12.0
-- Project Site: pgmodeler.io
-- Model Author: ---

-- object: app | type: ROLE --
-- DROP ROLE IF EXISTS app;
CREATE ROLE app WITH ;
-- ddl-end --


-- Database creation must be done outside a multicommand file.
-- These commands were put in this file only as a convenience.
-- -- object: hype_db | type: DATABASE --
-- -- DROP DATABASE IF EXISTS hype_db;
-- CREATE DATABASE hype_db;
-- -- ddl-end --
-- 

-- object: app | type: SCHEMA --
-- DROP SCHEMA IF EXISTS app CASCADE;
CREATE SCHEMA app;
-- ddl-end --
ALTER SCHEMA app OWNER TO app;
-- ddl-end --

SET search_path TO pg_catalog,public,app;
-- ddl-end --

-- object: app.drivers | type: TABLE --
-- DROP TABLE IF EXISTS app.drivers CASCADE;
CREATE TABLE app.drivers (
	id serial NOT NULL,
	user_id int4,
	operator_id int4 NOT NULL,
	driver_meta json,
	current_position text,
	is_waiting boolean DEFAULT false,
	created_at timestamp,
	updated_at timestamp,
	deleted_at timestamp,
	CONSTRAINT drivers_pk PRIMARY KEY (id)

);
-- ddl-end --
-- ALTER TABLE app.drivers OWNER TO postgres;
-- ddl-end --

-- object: app.passengers | type: TABLE --
-- DROP TABLE IF EXISTS app.passengers CASCADE;
CREATE TABLE app.passengers (
	id serial NOT NULL,
	user_id int4,
	otp varchar,
	passenger_meta json,
	current_position text,
	created_at timestamp,
	updated_at timestamp,
	deleted_at timestamp,
	CONSTRAINT passengers_pk PRIMARY KEY (id)

);
-- ddl-end --
-- ALTER TABLE app.passengers OWNER TO postgres;
-- ddl-end --

-- object: app.users | type: TABLE --
-- DROP TABLE IF EXISTS app.users CASCADE;
CREATE TABLE app.users (
	id serial NOT NULL,
	phone_no varchar,
	username varchar,
	email varchar,
	password varchar,
	salt varchar,
	first_name varchar,
	last_name varchar,
	date_of_birth date,
	gender varchar,
	city varchar,
	user_meta json,
	created_at timestamp,
	updated_at timestamp,
	deleted_at timestamp,
	CONSTRAINT users_pk PRIMARY KEY (id)

);
-- ddl-end --
-- ALTER TABLE app.users OWNER TO postgres;
-- ddl-end --


